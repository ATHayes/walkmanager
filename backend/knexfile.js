module.exports = {

  development: {
    client: 'postgresql',
    connection:'postgres://localhost/walkmanager',
    useNullAsDefault: true
  },

  production: {
    client: 'postgresql',
    connection: process.env.DATABASE_URL + '?ssl=true',
    useNullAsDefault: true
  }

};
