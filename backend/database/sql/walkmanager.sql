-- Rather than do db migrations, I'm just going to do everything here
-- The main reason is that heroku postgres doesn't play nicely with flyway (database migration tool of choice) outside of java
-- This script is idempotent

drop table if exists walk;
drop table if exists app_user;

create table walk (
    oid_walk serial primary key,
    name varchar(150) not null,
    img varchar(2083) not null,
    description varchar(3000),
    walk_date date not null,
    is_pickup boolean,
    pickup_location varchar(1000),
    start_location varchar(1000),
    end_location varchar(1000)
);

create table app_user (
  oid_user serial primary key,
  name varchar(150) not null
);