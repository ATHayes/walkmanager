#!/usr/bin/env bash
heroku pg:psql --app walkmanager-backend < ./sql/walkmanager.sql
heroku config:set PGSSLMODE=require --app walkmanager-backend