const knex = require("../../knex/knex.js");

export async function addWalk(walk) {
  let oidWalk = await knex("walk")
    .insert(walk)
    .returning("oid_walk");

  // can't return whole object from insertion above, have to query again
  return getWalk(oidWalk);
}

export async function getWalks() {
  return knex.from("walk").select().orderBy("oid_walk");
}

export async function getWalk(oidWalk) {
  return knex
    .from("walk")
    .select()
    .first()
    .where({ oid_walk: parseInt(oidWalk) });
}

export async function editWalk(walk) {
  let oidWalk = await knex("walk")
    .where("oid_walk", walk.oid_walk)
    .update(walk)
    .returning("oid_walk");

  return getWalk(oidWalk);
}

export async function deleteWalk(oid_walk) {
  return knex('walk')
      .where('oid_walk', parseInt(oid_walk))
      .del()
}
