import knex from "../../knex/knex.js";
import walks from "./walk-data.js";
import users from "./user-data.js";

export async function resetDb() {
  await clearWalkTable();
  await insertWalks();
  await clearUserTable();
  await insertUsers();
}

async function clearWalkTable() {
  await knex("walk").truncate();
}

async function insertWalks() {
  await knex("walk")
    .returning("*")
    .insert(walks);
}

async function clearUserTable() {
  await knex("app_user").truncate();
}

async function insertUsers() {
  await knex("app_user")
      .returning("*")
      .insert(users);
}
