export default [
  {
    "name": "Humber Valley",
    "img": "https://res.cloudinary.com/davbdgyux/image/upload/v1571087928/walkmanager/sylwia-bartyzel-LOt8O3nfZQ4-unsplash_thrzwg.jpg",
    "description": "Description 1",
    "walk_date": "2017-03-14T04:00:00.000Z",
    "is_pickup": false,
    "pickup_location": "",
    "start_location": "Humber Valley Car Park",
    "end_location": "Humber Valley Car Park"
  },
  {
    "name": "Evergreen Brickworks",
    "img": "https://res.cloudinary.com/davbdgyux/image/upload/v1571087920/walkmanager/evergreen_z3cfuf.jpg",
    "description": "Description 2",
    "walk_date": "2017-03-14T04:00:00.000Z",
    "is_pickup": true,
    "pickup_location": "North York Centre",
    "start_location": "Yonge and Eglinton",
    "end_location": "Evergreen Brickworks"
  },
  {
    "name": "High Park",
    "img": "https://res.cloudinary.com/davbdgyux/image/upload/v1571087919/walkmanager/cherry_blossoms_gmfila.jpg",
    "description": "Description 3",
    "walk_date": "2017-03-14T04:00:00.000Z",
    "is_pickup": false,
    "pickup_location": "",
    "start_location": "High Park Zoo",
    "end_location": "Toronto Waterfront"
  }
]
