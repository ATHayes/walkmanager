import {
  addWalk,
  deleteWalk,
  getWalk,
  getWalks,
  editWalk
} from "./walk/walk-service";
import bodyParser from "body-parser";
import cors from "cors";
import { addUser, getUser, getUsers } from "./user/user-service";
import { resetDb } from "./reset-db/reset-db";

const express = require("express");
const app = express();
const port = 3000;

app.use(cors());

// optional timeout for testing slow connections
// app.use(function(req,res,next){
//     setTimeout(next, 1500)
// });

// Parse body as JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.json()); // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies

// Main page - redirect to walks for now
app.get("/", (req, res) => res.redirect("/walks"));

// TODO refactor this into modules as per express best practices

// Walks
app.get("/walks", async (req, res) => res.send(await getWalks()));
app.get("/walk/:oid_walk", async (req, res) =>
  res.send(await getWalk(req.params.oid_walk))
);
app.put("/walk/:oid_walk", async (req, res) =>
  res.send(await editWalk(req.body))
); // Todo validate that oid walk in url matches the body id
app.post("/walk", async (req, res) => res.send(await addWalk(req.body)));
app.delete("/walk/:oid_walk", async (req, res) => {
  deleteWalk(req.params.oid_walk)
      .then(deleteCount => res.json({
        message: 'Walk deleted',
        deleteCount}))
      .catch();
});

// Reset data
app.get("/reset-db", async (req, res) => res.send(await resetDb()));

// Users
app.get("/users", async (req, res) => res.send(await getUsers()));
app.get("/user/:oid_user", async (req, res) =>
  res.send(await getUser(req.params.oid_user))
);
app.post("/user", async (req, res) => res.send(await addUser(req.body)));

app.listen(
  process.env.PORT || port, //allow for heroku port
  () => console.log(`WalkManager backend listening on port ${port}`)
);
