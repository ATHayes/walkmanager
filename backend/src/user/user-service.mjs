import knex from "../../knex/knex.js";
import {getWalk} from "../walk/walk-service";

export async function addUser(user) {
  let oidUser = await knex("app_user")
      .insert(user)
      .returning("oid_user");

  // can't return whole object from insertion above, have to query again
  return getUser(oidUser);
}

export async function getUsers() {
  return knex.from("app_user").select();
}

export async function getUser(oidUser) {
  return knex
      .from("app_user")
      .select()
      .first()
      .where({ oid_user: parseInt(oidUser) });
}

// export function putUser(oidUsecr, user) {
//   users[oidUser] = user;
//   users[oidUser].oidUser = oidUser; // preserve old id
//   return walks;
// }
//
// export function deleteUser(oidUser) {
//   users.splice(oidUser, 1);
//   return users;
// }
