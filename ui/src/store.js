import Vue from "vue";
import Vuex from "vuex";
import "es6-promise/auto";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    walks: [],
    users: [],
    selectedWalk: false,
    addWalkFormVisible: false,
    editWalkFormVisible: false,
    deleteWalkFormVisible: false
  },
  mutations: {
    addWalk(state, walk) {
      state.walks.push(walk);
    },
    editWalk(state, walk) {
      const existingWalk = this.getters.getWalkById(walk.oid_walk);
      Object.assign(existingWalk, walk);
    },
    getWalks(state, walks) {
      console.log(JSON.stringify(walks));
      state.walks = walks;
    },
    addUser(state, user) {
      state.users.push(user);
    },
    getUsers(state, users) {
      state.users = users;
    },
    setSelectedWalk(state, selectedWalk) {
      state.selectedWalk = selectedWalk;
    },
    setAddWalkFormVisible(state, bool) {
      state.addWalkFormVisible = bool;
    },
    setEditWalkFormVisible(state, bool) {
      state.editWalkFormVisible = bool;
    },
    setDeleteWalkFormVisible(state, bool) {
      state.deleteWalkFormVisible = bool;
    }
  },
  actions: {
    getWalks({ commit }) {
      axios
        .get(process.env.VUE_APP_BACKEND_URL + "/walks")
        .then(response => commit("getWalks", response.data));
    },
    addWalk({ commit }, walk) {
      axios
        .post(process.env.VUE_APP_BACKEND_URL + "/walk", walk)
        .then(response => commit("addWalk", response.data));
    },
    editWalk({ commit }, walk) {
      return axios
        .put(process.env.VUE_APP_BACKEND_URL + "/walk/" + walk.oid_walk, walk)
        .then(response => commit("editWalk", response.data));
    },
    async deleteWalk({ dispatch }, walk) {
      await axios.delete(
        process.env.VUE_APP_BACKEND_URL + "/walk/" + walk.oid_walk
      );
      dispatch("getWalks");
    },
    getUsers({ commit }) {
      axios
        .get(process.env.VUE_APP_BACKEND_URL + "/users")
        .then(response => commit("getUsers", response.data));
    },
    addUser({ commit }, user) {
      axios
        .post(process.env.VUE_APP_BACKEND_URL + "/user", user)
        .then(response => commit("addUser", response.data));
    },
    showEditWalkForm({ commit }, selectedWalk) {
      commit("setSelectedWalk", false);
      axios
        .get(process.env.VUE_APP_BACKEND_URL + "/walk/" + selectedWalk.oid_walk)
        .then(response => {
          commit("setSelectedWalk", response.data);
          commit("setEditWalkFormVisible", true);
        });
    },
    showDeleteWalkForm({ commit }, selectedWalk) {
      commit("setSelectedWalk", false);
      axios
        .get(process.env.VUE_APP_BACKEND_URL + "/walk/" + selectedWalk.oid_walk)
        .then(response => {
          commit("setSelectedWalk", response.data);
          commit("setDeleteWalkFormVisible", true);
        });
    },
    async resetData({ dispatch }) {
      await axios.get(process.env.VUE_APP_BACKEND_URL + "/reset-db");
      dispatch("getWalks");
    }
  },
  getters: {
    getWalkById: state => id => {
      return state.walks.find(walk => walk.oid_walk === id);
    }
  }
});
