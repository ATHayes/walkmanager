import Vue from "vue";
import Router from "vue-router";
import WalkList from "./views/WalkList";
import Users from "./views/Users";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "walks",
      component: WalkList
    },
    {
      path: "/walks",
      name: "walks",
      component: WalkList
    },
    {
      path: "/users",
      name: "users",
      component: Users
    }

  ]
});
