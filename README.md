# WalkManager application

[live demo](http://walkmanager.athayes.me)

This is a simple project to practice writing full stack javascript.

The walk manager application is designed for people who manage walking groups. It allows them to add walks, manage users and add users to walks.

This gives us a nice many-to-many relationship to play around with (walks - participants) and an opportunity to sneak some cool pictures in.

Project goals:
* As much vanilla javascript (es6) as possible.
* Deployed to real (hopefully free) hosting.

## Deployment:
1. Backend: Deployed on Heroku on merge with Master, using Gitlab CI/CD: https://walkmanager-backend.herokuapp.com/walks. Note that Heroku may take some time to boot up once you call it, so be patient.
2. Frontend: Deployed on Netlify on merge with Master, using Netlify CI/CD: https://heuristic-tereshkova-dcb4f9.netlify.com/
3. Photos: Stored on [Cloudinary](https://cloudinary.com/about) (an Amazon S3 equivilant).

## Manual steps when setting up from scratch:
1. Create heroku app
2. Create heroku postgres addon
